min([Value], Value).
min([Value | List], Value) :- 
	min(List, TempResult),
	Value =< TempResult, !.
min([_Item | List], Result) :-
min(List, Result).