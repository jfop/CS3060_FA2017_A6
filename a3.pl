nonStopFlight(pittsburgh,cleveland).
nonStopFlight(philadelphia,pittsburgh).
nonStopFlight(columbus,philadelphia).
nonStopFlight(sanFrancisco,columbus).
nonStopFlight(detroit,sanFrancisco).
nonStopFlight(toledo,detroit).
nonStopFlight(houston,toledo).

route(W,Z):-nonStopFlight(W,Z).
route(W,Z):-nonStopFlight(W,Y),route(Y,Z).
