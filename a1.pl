reverse(List, Value) :-
        reverse(List, Value, []).

reverse([], L, L).
reverse([H|T], L, Remain) :-
        reverse(T, L, [H|Remain]).
